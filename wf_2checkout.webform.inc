<?php

/**
 * @file
 * Webform module wf_2checkout component.
 */

/**
 * Implements _webform_defaults_component().
 */
function _webform_defaults_wf_2checkout() {
  return [
    'name' => '',
    'form_key' => NULL,
    'pid' => 0,
    'weight' => 0,
    'value' => '',
    'extra' => [
      'amount' => 0,
      'amount_component' => '',
      'select_email' => '',
      'email_component' => '',
      'pay_button' => '',
      'private' => FALSE,
    ],
  ];
}

/**
 * Implements _webform_edit_component().
 */
function _webform_edit_wf_2checkout($component) {
  $node = node_load($component['nid']);
  $form = [];
  // Mapping exists field of amount.
  $form['amount'] = [
    '#type' => 'fieldset',
    '#title' => t('Amount'),
    '#collapsible' => TRUE,
    '#tree' => TRUE,
    '#parents' => ['extra'],
  ];
  // Checking exists fields for amount.
  $amount_components = [];
  foreach ($node->webform['components'] as $other_component) {
    $component_type = $other_component['type'];
    if (in_array($component_type, ['textfield', 'number', 'select'])) {
      if ($component_type === 'select') {
        continue;
      }
      $amount_components[$other_component['cid']] = check_plain($other_component['name']);
    }
  }
  // Dynamic component.
  $form['amount']['amount_component'] = [
    '#type' => 'select',
    '#title' => t('Amount Field'),
    '#description' => t("Select a dynamic amount field using previously fields and the component will determine the amount."),
    '#options' => $amount_components,
    '#empty_option' => t('Select a component'),
    '#default_value' => $component['extra']['amount_component'],
  ];
  // Email field.
  $form['email'] = [
    '#type' => 'fieldset',
    '#title' => t('E-mail'),
    '#collapsible' => TRUE,
    '#tree' => TRUE,
    '#parents' => ['extra'],
  ];
  // Select email options.
  $form['email']['select_email'] = [
    '#type' => 'radios',
    '#title' => t('E-mail Address'),
    '#options' => [
      'user' => t('Logged-in user email address'),
      'component' => t('Dynamic email address'),
    ],
    '#default_value' => $component['extra']['select_email'],
    '#required' => TRUE,
  ];
  // Dynamic email component.
  $email_component = [];
  foreach ($node->webform['components'] as $related_component) {
    if (in_array($related_component['type'], ['email'])) {
      $email_component[$related_component['cid']] = $related_component['name'];
    }
  }
  $form['email']['email_component'] = [
    '#type' => 'select',
    '#title' => t('Dynamic email address'),
    '#description' => t("Sets the email component which is used for the email address. Eligible components."),
    '#options' => $email_component,
    '#empty_option' => t('Select component'),
    '#default_value' => $component['extra']['email_component'],
    '#states' => [
      'visible' => [
        ':input[name="extra[select_email]"]' => ['value' => 'component'],
      ],
    ],
  ];
  // Pay button.
  $form['form_button'] = [
    '#type' => 'fieldset',
    '#title' => t('Submit Button'),
    '#collapsible' => TRUE,
    '#tree' => TRUE,
    '#parents' => ['extra'],
  ];
  $form['form_button']['pay_button'] = [
    '#type' => 'textfield',
    '#title' => t('Button Label'),
    '#description' => t('This button will be replaced with default webform submit button. Default: <em>@default</em>',
      ['@default' => 'Pay Now']
    ),
    '#default_value' => $component['extra']['pay_button'],
    '#parents' => ['extra', 'pay_button'],
  ];
  return $form;
}

/**
 * Implements _webform_render_component().
 */
function _webform_render_wf_2checkout($component, $value = NULL, $filter = TRUE, $submission = NULL) {
  // This is a hidden field which will store Order Number for success payment.
  $element = [
    '#type' => 'hidden',
    '#attributes' => ['class' => 'wf_2checkout_order_number'],
    '#theme_wrappers' => ['webform_element'],
    '#default_value' => $component['value'],
    '#webform_component' => $component,
  ];
  return $element;
}

/**
 * Implements _webform_display_component().
 */
function _webform_display_wf_2checkout($component, $value, $format = 'html', $submission = []) {
  if (isset($value[0])) {
    return [
      '#type' => 'markup',
      '#title' => $component['name'] . ' ' . t('Reference ID'),
      '#markup' => $value[0],
      '#weight' => $component['weight'],
      '#theme_wrappers' => $format == 'html' ? ['webform_element'] : ['webform_element_text'],
      '#translatable' => ['title'],
    ];
  }
}

/**
 * Implements _webform_table_component().
 */
function _webform_table_wf_2checkout($component, $value) {
  if (empty($value[0])) {
    return;
  }
  else {
    return ucfirst($value[0]);
  }
}

/**
 * Implements _webform_csv_headers_component().
 */
function _webform_csv_headers_wf_2checkout($component, $export_options) {
  $header = [];
  $header[0] = '';
  $header[1] = '';
  $header[2] = $component['name'];
  return $header;
}

/**
 * Implements _webform_csv_data_component().
 */
function _webform_csv_data_wf_2checkout($component, $export_options, $value) {
  if (empty($value[0])) {
    return;
  }
  else {
    return ucfirst($value[0]);
  }
}
